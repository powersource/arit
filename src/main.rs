#[macro_use]
extern crate iron;
extern crate router;

use iron::prelude::*;
use iron::status;
use iron::error::HttpError;
use router::Router;

fn main() {
    let mut router = Router::new();
    router.get("/", root, "root")
        .get("/:num1/:op/:num2", arit, "arit");

    Iron::new(router).http("localhost:4432").unwrap();
}

fn root(_: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, "Hello World!")))
}

fn arit(req: &mut Request) -> IronResult<Response> {
    let params = req.extensions.get::<Router>().unwrap();
    
    // Using ::Method feels wrong but I couldn't really find a better alternative, and I
    // think I had to pick one. Maybe io?
    let num1 = itry!(params.find("num1").unwrap().parse::<i32>().or(Err(HttpError::Method)), status::BadRequest);
    let op = params.find("op").unwrap();
    let num2 = itry!(params.find("num2").unwrap().parse::<i32>().or(Err(HttpError::Method)), status::BadRequest);

    let result = itry!(match op {
        "div" => Ok(num1 / num2),
        "mul" => Ok(num1 * num2),
        "sub" => Ok(num1 - num2),
        "add" => Ok(num1 + num2),
        _ => Err(HttpError::Method),
    }, status::BadRequest);
    Ok(Response::with((status::Ok, format!("{} {} {} = {}", num1, op, num2, result))))
}